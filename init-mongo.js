db.auth('__MYSQL_DBUSER__', '__MYSQL_DBPASS__')

db = db.getSiblingDB('dev_db')

db.createUser({
    user: "developer",
    pwd: "devPass",
    roles: [
        {
            role: "readWrite",
            db: "dev_db"
        }
    ]
});