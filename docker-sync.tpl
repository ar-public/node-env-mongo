version: 2
options:
  verbose: true

syncs:
  __PREFIX__project-sync:
    src: './project'
    sync_excludes: ['runtime']
    notify_terminal: true
