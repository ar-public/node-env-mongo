# Пропишем версию
version: '3.3'
# Перечислим сервисы
services:

    __PREFIX__db:

        image: mongo

        ports:
            - "27017:27017"

        container_name: __PREFIX__db
        environment:
            - MONGO_INITDB_ROOT_USERNAME=__MYSQL_DBUSER__
            - MONGO_INITDB_ROOT_PASSWORD=__MYSQL_DBPASS__
            - MONGO_INITDB_DATABASE=__MYSQL_DBNAME__
        volumes:
            - ./docker/database/data:/data/db
            - ./docker/database/init:/docker-entrypoint-initdb.d:ro


    __PREFIX__node:
        image: node:latest
        ports:
            - "1234:1234"
        container_name: __PREFIX__node
        command: "npm start"
        volumes:
            - ./project:/usr/src/service
        working_dir: /usr/src/service
        links:
            - __PREFIX__db:db


volumes:
    db-data:
        driver: local
